# sqlformat.el

Provides the `sqlformat` command, which uses the external Python tool `sqlformat` to format SQL in the current buffer.

Install `sqlformat` (contained in the `sqlparse` package) with `pip`:

    pip install sqlparse

and install `sqlformat.el` with `quelpa`:

```cl
(quelpa '(sqlformat :fetcher github :repo "steckerhalter/sqlformat.el"))
```

Bind the `sqlformat` command to a key:

```cl
(global-set-key (kbd "C-h s") 'sqlformat)
```

Be aware that if the buffer is mixed code/SQL then you need to select the SQL so that `sqlformat` only operates on the region, otherwise it acts on the whole buffer.
